// Bootstrap grid system components (row, col)
import { Row, Col } from 'react-bootstrap';

export default function Banner({ header, text, extras}) {
	return (

		<Row>
			<Col className="p-5">
				<h1>{header}</h1>
				<p>{text}</p>
				<div>{extras}</div>
			</Col>
		</Row>
	);
}