import Banner from '../components/Banner';
// import CourseCard from '../components/CourseCard'; will be imported to Courses page
import Highlights from '../components/Highlights';
import { Button } from 'react-bootstrap'

export default function Home() {


		const header = "Zuitt Coding Bootcamp"
		const text = "Opportunities for everyone, everywhere."

	return (
		<>
			<Banner header={header} text={text} extras={<Button variant="primary">Enroll Now!</Button>}/>
			
			<Highlights />
			{/*<CourseCard />*/}
		</>
	)
}