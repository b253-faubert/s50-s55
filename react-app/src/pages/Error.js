import Banner from '../components/Banner';

export default function Error() {
	
	const header = "Page Not Found"
	return (
		<>
			<Banner header={header} text={<span>Go back to <a href="/">homepage</a></span>}/>
		</>
	)
}